# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [2018-10-15]

### Added

- Added CI integration
- Added CI status

### Changed

- ..just some other very small updates

## [2017-12-18]

### Changed

- Changes as requested by teacher.
- Final version (as teacher accepted).

### Fixed

- Fixed working with negative weigths (lengths).
- Improved work on possible infinite loops (that's negatives).

## [2017-12-17]

### Changed

- Improved documentation.
- GIT now available publicly.

## [2017-12-16]

### Added

- Find Shortest Route and Save Results added.
- Program is now completely working! Just some changes need to be done tomorrow.

### Fixed

- Fixed Reading function, it threw some off data. Also, simplified a bit
- Fixed first Drawing function, made it a bit nicer also.
- Checked and fixed second Drawing function - was not working before. Also it worked incorrectly for route search.
- Fixed Changelog a bit (again).

## [2017-12-14]

### Added

- Draw functions added.

### Changed

- UI changed again, made window bigger and now more room for drawing graph.
- Some basic "status" output in the top left TextBox.

## [2017-12-12]

### Added

- Some basic functionality.
- Finally available to compile and run this thing.
- Reading file is now available.
- About window.

### Changed

- UI pretty much complete now, might need few tweaks later.

## [2017-12-09]

### Added

- First few files added.
- Starting UI interface.
- Added README.md.

### Changed

- Changed CHANGELOG to CHANGELOG.md - keep it in the same Markdown style.