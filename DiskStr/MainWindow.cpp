/** 22.	Sudaryti algoritm� ir program�, randan�i� trumpiausi� keli� svoriniame grafe, kurio briaun� svoriai gali b�ti ir neigiami.
 *  Mantas Jokubauskas, IFF-6/2
 *  For Diskre�iosios Strukt�ros, P170B008
 *  Using English so that most stuff can be understood by people abroad, if needed.
 *  GIT: https://gitlab.com/mantazzo/DiskStr
 */

#include "MainWindow.h"
#include <sstream>

using namespace System;
using namespace System::Windows::Forms;
//---------------------------------------------
[STAThread]
/** Main
 * Makes the app working
 */
int main(array <System::String ^> ^args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	DiskStr::MainWindow form;
	Application::Run(%form);
	Application::Exit();
}
//---------------------------------------------

//---------------------------------------------
/** Reading the file data and saving it to arrays.
 * @param fv file name
 * @param array_L L array
 * @param array_Lst1 lst array, address 1
 * @param array_Lst2 lst array, address 2
 * @param array_C C array
 * @param k number of Points
 * @param n number of connections
 */
void ReadGraphData(const std::string fv, int *array_L, int *array_Lst1, int *array_Lst2, int *array_C, int & k, int & n)
{
	std::ifstream fd(fv.c_str());
	std::string str;
	int act = 1;            //Action (used below)
	bool end = fd.eof();
	while (!end)
	{
		int i = 0;
		if (act==1)         //First line of data interpreted as L array (Action 1)
		{
			getline(fd, str);
			std::istringstream ss(str);
			int num;
			while (ss >> num)
			{
				array_L[i] = num;
				i++;
			}
			act++;
			k = i;
		}
		else if (act == 2)  //Second line of data interpreted as lst array, connection Point 1 (Action 2)
		{
			getline(fd, str);
			std::istringstream ss(str);
			int num;
			while (ss >> num)
			{
				array_Lst1[i] = num;
				i++;
			}
			act++;
			n = i;
		}
		else if (act == 3)  //Third line of data interpreted as lst array, connection Point 2 (Action 3)
		{
			getline(fd, str);
			std::istringstream ss(str);
			int num;
			while (ss >> num)
			{
				array_Lst2[i] = num;
				i++;
			}
			act++;
		}
		else if (act == 4)  //Fourth line of data interpreted as C array (values of connections) (Action 4)
		{
			getline(fd, str);
			std::istringstream ss(str);
			int num;
			while (ss >> num)
			{
				array_C[i] = num;
				i++;
			}
			end = true;     //We don't need more info after this
		}
	}
	fd.close();
}
//---------------------------------------------
/** Drawing the graph on screen.
* @param graphics Graphics creator (access)
* @param array_Lst1 lst array, address 1
* @param array_Lst2 lst array, address 2
* @param array_C C array
* @param x x coordinates array
* @param y y coordinates array
* @param k number of Points
* @param n number of connections
* @param w working space width
* @param h working space heigth
* @param size Point size
*/
void DrawGraph(System::Drawing::Graphics^ graphics,int *array_Lst1, int *array_Lst2, int *array_C, int *x, int *y, int k, int n, int w, int h, int size)
{
	srand(time(NULL));            //random seed generating
	int limit_w = w - size;       //working size, limited so Point wouldn't go out of bounds
	int limit_h = h - size;       //working size, limited so Point wouldn't go out of bounds
	/* Draw Points randomly*/
	for (int i=1; i<k; i++)
	{
		int draw_x = rand() % limit_w + 0;
		int draw_y = rand() % limit_h + 0;

		x[i] = draw_x;
		y[i] = draw_y;

		graphics->FillEllipse(System::Drawing::Brushes::Red, draw_x, draw_y, size, size);
		graphics->DrawEllipse(System::Drawing::Pens::Black, draw_x, draw_y, size, size);
	}
	/* Connect points*/
	for (int i=0; i<n; i++)
	{
		//Draw connection line
		graphics->DrawLine(System::Drawing::Pens::Red, x[array_Lst1[i]]+(size / 2), y[array_Lst1[i]] + (size / 2), x[array_Lst2[i]] + (size / 2), y[array_Lst2[i]] + (size / 2));
		int c_position_x = (x[array_Lst1[i]] + x[array_Lst2[i]]) / 2;
		int c_position_y = (y[array_Lst1[i]] + y[array_Lst2[i]]) / 2;
		auto koef = System::Convert::ToString(array_C[i]);
		//Draw the connection "weight"
		graphics->DrawString(koef,
			gcnew System::Drawing::Font("Arial", 8, System::Drawing::FontStyle::Bold), System::Drawing::Brushes::Green, System::Drawing::Point::Point(c_position_x - 3, c_position_y - 6));
		//Works for now
		//Potential for Drawing koef in other styles depending on positions, may do it later
	}
	/* Draw Point numbers on top*/
	for (int i=1; i<k; i++)
	{
		int draw_x = x[i];
		int draw_y = y[i];
		auto s = System::Convert::ToString(i);
		graphics->DrawString(s,
			gcnew System::Drawing::Font("Arial", 8, System::Drawing::FontStyle::Bold), System::Drawing::Brushes::Black, System::Drawing::Point::Point(draw_x + 5, draw_y + 5));
	}
}
//---------------------------------------------
/** Calculating shortest routes from start point.
* @param start Start point
* @param array_L L array
* @param array_Lst2 lst array, address 2
* @param array_C C array
* @param k number of Points
* @param dst distance array (d)
* @param prev previous Point array (prec)
* @param ptd logical array for painted Points
* @param repaint array for calculating number of times for repainting (used when there are negative numbers)
*/
void CalculateShortestRoutes(int start, int *array_L, int *array_Lst2, int *array_C, int k, int *dst, int *prev, bool *ptd, int *repaint)
{
	int dst_allowed = 10000;       //for distance calculations
	int dst_max = 0;               //for next point calculations
	bool finished = false;         //logic for "while"
	prev[start] = start;           //saving start data
	dst[start] = 0;                //distance for start point is 0
	ptd[start] = true;             //setting logical "painted" to true for start point
	int no = start;                //starting number
	int lim1 = 0;                  //lower limit
	int lim2 = 0;                  //upper limit
	int pt = 0;                    //working point
	int d_sum = 0;
	int no_tmp = 0;                //temporary save for number
	int ptd_no = 1;                //painted amount
	while (!finished)
	{
		no_tmp = 0;
		dst_max = dst_allowed;
		lim1 = array_L[no-1];              //working addresses, lower limit
		lim2 = array_L[no]-1;              //working addresses, upper limit
		for (int i=lim1; i<=lim2; i++)
		{
			pt = array_Lst2[i];         //working point
			//check if Point is painted
			if (ptd[pt] != true)
			{
				d_sum = dst[no] + array_C[i];
				if (d_sum < dst[pt])
				{
					//if new calculated sum is smaller than current one then save it
					dst[pt] = d_sum;
					prev[pt] = no;
				}
			}
			//work a bit differently when there are negatives (Point can be repainted)
			else
			{
				d_sum = dst[no] + array_C[i];
				if (d_sum < dst[pt])
				{
					//check if point has been repainted a few times already (prevents infinite loop)
					if (repaint[pt] < 3)
					{
						//if new calculated sum is smaller than current one then save it
						dst[pt] = d_sum;
						prev[pt] = no;
						ptd[pt] = false;
						ptd_no--;
						repaint[pt] = repaint[pt] + 1;
					}
				}
			}
		}
		for (int i = 1; i<k; i++)
		{
			//search for smallest unpainted Point (lowest distance)
			if (dst[i]<dst_max)
			{
				if (ptd[i] == false)
				{
					dst_max = dst[i];
					no_tmp = i;
				}
			}
		}
		no = no_tmp;           //set next Point
		ptd[no_tmp] = true;    //set next Point painted
		ptd_no++;
		if (ptd_no == k-1)
			finished = true;
	}
}
//---------------------------------------------
/** Save the shortest route from End Point to Start Point.
* @param start Start Point
* @param finish End Point
* @param graphics Graphics creator (access)
* @param prev previous Points array (prec)
* @param x x coordinates array
* @param y y coordinates array
* @param route Shortest Route string
* @param size Point size
* @param touched logical array saving if the point was used (array used to prevent infinite loops)
*/
void OutputShortestRoute(int start, int finish, System::Drawing::Graphics^ graphics, int *prev, int *x, int *y, std::string & route, int size, bool *touched)
{
	bool finished = false;
	int pt = finish;            //starting point as End Point
	std::string tmp;            //temporary string
	route = std::to_string(pt);
	route += " ";
	touched[pt] = true;
	while (!finished)
	{
		int next = 0;
		next = prev[pt];
		//draw line between points in Blue
		graphics->DrawLine(System::Drawing::Pens::Blue, x[pt] + (size / 2), y[pt] + (size / 2), x[next] + (size / 2), y[next] + (size / 2));
		if (prev[next] == pt && touched[pt] == true && touched[next] == true)
		{
			//infinite loop detected
			finished = true;
			route = "Impossible (Infinite loop detected)";
		}
		else
		{
			pt = next;
			//check if previous Point is now equal to the point number itself (or to the start point)
			if (prev[pt] == pt || pt == start)
			{
				finished = true;
				tmp = std::to_string(pt);
				route += tmp;
				touched[pt] = true;
			}
			else
			{
				tmp = std::to_string(pt);
				tmp += " ";
				route += tmp;
				touched[pt] = true;
			}
		}
	}
}
//---------------------------------------------
/** Output the start data and shortest route between points.
* @param fv file name
* @param array_L L array
* @param array_Lst1 lst array, address 1
* @param array_Lst2 lst array, address 2
* @param array_C C array
* @param n number of connections
* @param k number of Points
* @param route Shortest Route
* @param comm comment in the beginning
* @param start Start Point
* @param finish End Point
*/
void GenerateResults(const std::string fv, int *array_L, int *array_Lst1, int *array_Lst2, int *array_C, int n, int k, std::string route, std::string comm, int start, int finish)
{
	//Add comment
	std::ofstream fr(fv.c_str(), std::ios::app);
	fr << comm << std::endl;
	fr << std::endl;
	//Generate L array
	fr << "|----------------------------------------------------------" << std::endl;
	fr << "|                           L                              " << std::endl;
	fr << "|----------------------------------------------------------" << std::endl;
	fr << "| ";
	for (int i = 0; i<k; i++)
	{
		std::string convert = std::to_string(array_L[i]);
		fr << std::setw(3) << convert << " ";
	}
	fr << std::endl;
	fr << "|----------------------------------------------------------" << std::endl;
	fr << std::endl;
	//Generate lst matrix and C array
	fr << "|----------------------------------------------------------" << std::endl;
	fr << "|                        lst + C                           " << std::endl;
	fr << "|----------------------------------------------------------" << std::endl;
	fr << "| ";
	for (int i = 0; i < n; i++)
	{
		std::string convert = std::to_string(array_Lst1[i]);
		fr << std::setw(3) << convert << " ";
	}
	fr << std::endl;
	fr << "| ";
	for (int i = 0; i < n; i++)
	{
		std::string convert = std::to_string(array_Lst2[i]);
		fr << std::setw(3) << convert << " ";
	}
	fr << std::endl;
	fr << "|----------------------------------------------------------" << std::endl;
	fr << "| ";
	for (int i = 0; i < n; i++)
	{
		std::string convert = std::to_string(array_C[i]);
		fr << std::setw(3) << convert << " ";
	}
	fr << std::endl;
	fr << "|----------------------------------------------------------" << std::endl;
	fr << std::endl;
	std::string tmp_start = std::to_string(start);
	std::string tmp_finish = std::to_string(finish);
	//Output the shortest route
	fr << "The Shortest Route between Point " << tmp_start << " and Point " << tmp_finish << " is this route (backwards):" << std::endl;
	fr << route << std::endl;
	fr.close();
}
//---------------------------------------------
/** Convert s (String ^) to os (string) */
 void DiskStr::StringTostring(String ^ s, std::string & os)
{
	using namespace Runtime::InteropServices;
	const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}