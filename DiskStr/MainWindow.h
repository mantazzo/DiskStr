#pragma once
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

int L[10];       //L array for saving size of connections
int Lst1[100];   //lst array for saving connection points, point 1
int Lst2[100];   //lst array for saving connection points, point 2
int C[100];      //C array for saving weigths
int x[10];       //x array for saving coordinates for drawing, x axis
int y[10];       //y array for saving coordinates for drawing, y axis
int k;           //k for saving the number of points
int n;           //n for saving the number of connections
int size = 20;   //Point size when drawing
std::string route;  //route for saving Shortest Route

void ReadGraphData(const std::string fv, int *array_L, int *array_Lst1, int *array_Lst2, int *array_C, int & k, int & n);
void DrawGraph(System::Drawing::Graphics^ graphics,int *array_Lst1, int *array_Lst2, int *array_C, int *x, int *y, int k, int n, int w, int h, int size);
void CalculateShortestRoutes(int start, int *array_L, int *array_Lst2, int *array_C, int k, int *dst, int *prev, bool *ptd, int *repaint);
void OutputShortestRoute(int start, int finish, System::Drawing::Graphics^ graphics, int *prev, int *x, int *y, std::string & route, int size, bool *touched);
void GenerateResults(const std::string fv, int *array_L, int *array_Lst1, int *array_Lst2, int *array_C, int n, int k, std::string route, std::string comm, int start, int finish);

namespace DiskStr {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	//---------------------------------------------
	//using namespace std;    // doesn't work correctly
	//---------------------------------------------

	//---------------------------------------------
	void StringTostring(String ^ s, std::string & os);
	//---------------------------------------------

	/// <summary>
	/// Summary for MainWindow
	/// </summary>
	public ref class MainWindow : public System::Windows::Forms::Form
	{
	public:
		MainWindow(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainWindow()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::RichTextBox^  richTextBox1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Label^  label2;


	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(240, 50);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(60, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Output Log";
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->fileToolStripMenuItem,
					this->helpToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(984, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->exitToolStripMenuItem });
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(92, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::exitToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->aboutToolStripMenuItem });
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->aboutToolStripMenuItem->Text = L"About...";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::aboutToolStripMenuItem_Click);
			// 
			// richTextBox1
			// 
			this->richTextBox1->BackColor = System::Drawing::Color::White;
			this->richTextBox1->Font = (gcnew System::Drawing::Font(L"Calibri", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(186)));
			this->richTextBox1->Location = System::Drawing::Point(20, 70);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->ReadOnly = true;
			this->richTextBox1->Size = System::Drawing::Size(500, 150);
			this->richTextBox1->TabIndex = 2;
			this->richTextBox1->Text = L"Nothing yet...";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(694, 100);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(132, 23);
			this->button1->TabIndex = 3;
			this->button1->Text = L"Load File...";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainWindow::button1_Click);
			// 
			// button2
			// 
			this->button2->Enabled = false;
			this->button2->Location = System::Drawing::Point(694, 249);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(132, 23);
			this->button2->TabIndex = 4;
			this->button2->Text = L"Draw";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MainWindow::button2_Click);
			// 
			// button3
			// 
			this->button3->Enabled = false;
			this->button3->Location = System::Drawing::Point(694, 550);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(132, 23);
			this->button3->TabIndex = 5;
			this->button3->Text = L"Find Shortest Route";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MainWindow::button3_Click);
			// 
			// button4
			// 
			this->button4->Enabled = false;
			this->button4->Location = System::Drawing::Point(694, 700);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(132, 23);
			this->button4->TabIndex = 6;
			this->button4->Text = L"Save Results...";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MainWindow::button4_Click);
			// 
			// panel1
			// 
			this->panel1->Location = System::Drawing::Point(20, 249);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(500, 500);
			this->panel1->TabIndex = 7;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(253, 228);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(36, 13);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Graph";
			// 
			// label3
			// 
			this->label3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(619, 375);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(56, 13);
			this->label3->TabIndex = 11;
			this->label3->Text = L"Start Point";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(834, 375);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(53, 13);
			this->label4->TabIndex = 12;
			this->label4->Text = L"End Point";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(622, 425);
			this->textBox1->MaxLength = 4;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(53, 20);
			this->textBox1->TabIndex = 13;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(837, 425);
			this->textBox2->MaxLength = 4;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(50, 20);
			this->textBox2->TabIndex = 14;
			// 
			// MainWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(984, 761);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->richTextBox1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MainWindow";
			this->Text = L"Diskrečiosios struktūros";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		/** Open File dialog
		 * Opens selected file and reads the data
		 */
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	richTextBox1->Clear();
	richTextBox1->Text = "Opening new file...";
	// Starting openFileDialog
	openFileDialog1->DefaultExt = ".txt";
	openFileDialog1->Filter = "TXT files|*.txt";
	openFileDialog1->FileName = ".txt";
	openFileDialog1->Title = "Open File...";
	// Check if openFileDialog is not empty
	if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK && openFileDialog1->FileName->Length > 0) {
		String^ dfv; // input file name (String ^)
		dfv = openFileDialog1->FileName;
		// Converting file name dfv (String ^) to dfvs (string)
		std::string dfvs; // input file name (string)
		StringTostring(dfv, dfvs);
		// Reading the file now
		ReadGraphData(dfvs, L, Lst1, Lst2, C, k, n);
		richTextBox1->Text = "File successfully opened!";
		button1->Enabled = false;
		button2->Enabled = true;          //Draw button enabled
		button3->Enabled = false;
		button4->Enabled = false;
	}
	else richTextBox1->Text = "File opening cancelled.";
}
		/** Draw Graph
		 * Draws the graph
		 */
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	if (panel1->BackgroundImage == nullptr)
	{
		panel1->BackgroundImage = gcnew System::Drawing::Bitmap(panel1->Width, panel1->Height);
	}

	Graphics^ buffGraphics = Graphics::FromImage(panel1->BackgroundImage);

	int w = panel1->Width;       //allowed space to draw
	int h = panel1->Height;      //allowed space to draw

	buffGraphics->Clear(panel1->BackColor);

	DrawGraph(buffGraphics, Lst1, Lst2, C, x, y, k, n, w, h, size);

	panel1->Update();
	richTextBox1->Text = "Graph has been successfully drawn below!";
	button1->Enabled = false;
	button2->Enabled = false;    //Don't re-draw multiple times
	button3->Enabled = true;
	button4->Enabled = false;
}
		/** Find Shortest Route
		 * Finds shortest route between 2 given points.
		 */
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	int prec[10];     //for calculating shortest route, previous point
	int d[10];        //for calculating shortest route, route length from start point
	bool painted[10]; //for calculating shortest route, saves if the point was used already
	int repainted[10];  //how many times Point has been repainted
	bool touched[10];   //if point was used (for shortest route output)
	int m1;           //Start Point
	int m2;           //End Point
	int c_sum = 0;    //Temporary to set up distance calculation
	int lmt = 8;
	//Check if textBox is empty, yes - throw error, no - continue
	if (textBox1->Text == "" || textBox1->Text == "")
	{
		MessageBox::Show(L"Error!\nAt least one field has no input!",
			L"Error - Missing Point", MessageBoxButtons::OK, MessageBoxIcon::Error);
		DialogResult = Windows::Forms::DialogResult::None;
		richTextBox1->Text = "Error - Missing Point input.";
	}
	else
	{
		m1 = int::Parse(textBox1->Text);         //Start Point
		m2 = int::Parse(textBox2->Text);         //End Point
		//Check various limits
		if (m1 < 1 || m2 < 1)
		{
			MessageBox::Show(L"Error!\nAt least one entered Point is less than 1!\nPoints always start from 1.",
				L"Error - Too Low", MessageBoxButtons::OK, MessageBoxIcon::Error);
			DialogResult = Windows::Forms::DialogResult::None;
			richTextBox1->Text = "Error - At least one Point input is too low.";
		}
		else if (m1 > lmt || m2 > lmt)
		{
			MessageBox::Show(L"Error!\nAt least one entered Point is bigger than the total amount of Points!",
				L"Error - Too High", MessageBoxButtons::OK, MessageBoxIcon::Error);
			DialogResult = Windows::Forms::DialogResult::None;
			richTextBox1->Text = "Error - At least one Point input is too high.";
		}
		//Check if values are the same
		else if (m1 == m2)
		{
			MessageBox::Show(L"Error!\nStart and End points can't be equal (same number)!",
				L"Error - Equal", MessageBoxButtons::OK, MessageBoxIcon::Error);
			DialogResult = Windows::Forms::DialogResult::None;
			richTextBox1->Text = "Error - Point inputs can't be equal.";
		}
		//If nothing is wrong, continue
		else
		{
			/*for (int i=0; i<n; i++)
			{
				c_sum = c_sum + C[i];
			}*/
			//c_sum = c_sum / 2;           //not really needed since we might have negative
			//c_sum = c_sum + 1;
			c_sum = 10000;                 //random high c_sum value, since we're working with negative numbers
			//Set up the new arrays
			for (int i = 0; i < 10; i++)
			{
				prec[i] = 0;
				d[i] = c_sum;
				painted[i] = false;
				repainted[i] = 0;
				touched[i] = false;
			}

			Graphics^ buffGraphics = Graphics::FromImage(panel1->BackgroundImage);

			CalculateShortestRoutes(m1, L, Lst2, C, k, d, prec, painted, repainted);
			OutputShortestRoute(m1, m2, buffGraphics, prec, x, y, route, size, touched);
			panel1->Refresh();
			richTextBox1->Text = "Shortest Route between given Points has been found!\nConnection lines are shown below in Blue.";
			button1->Enabled = false;
			button2->Enabled = false;
			button3->Enabled = false;
			button4->Enabled = true;
		}
	}
}
		/** Save File dialog
		 * Saves the Results data into a txt file
		 */
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	richTextBox1->Text = "Saving Results...";
	// Starting saveFileDialog
	saveFileDialog1->DefaultExt = ".txt";
	saveFileDialog1->Filter = "TXT files|*.txt";
	saveFileDialog1->Title = "Save Results...";
	saveFileDialog1->FileName = "Results.txt";
	// Check if SaveFileDialog is not empty (file selected)
	if (saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK && saveFileDialog1->FileName->Length > 0) {
		String^ rfv; // output file name (String ^)
		rfv = saveFileDialog1->FileName;
		// Converting rfv (String ^) to rfvs (string)
		std::string rfvs; // output file name (string)
		StringTostring(rfv, rfvs);
		// Creating empty file
		std::ofstream fr(rfvs.c_str());
		fr.close();
		//Finally, let's output the information
		int m1 = int::Parse(textBox1->Text);         //Start Point
		int m2 = int::Parse(textBox2->Text);         //End Point
		GenerateResults(rfvs, L, Lst1, Lst2, C, n, k, route, "Given data:", m1, m2);
		richTextBox1->Text = "Results were succesfully saved!";
		button1->Enabled = false;
		button2->Enabled = false;
		button3->Enabled = false;
		button4->Enabled = false;
	}
	else richTextBox1->Text = "Results were not saved.";
}
		/** Additional Close option */
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
		/** Small About window */
private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	MessageBox::Show(L"Created for Diskrečiosios struktūros specifically.\nCopyright © Mantas Jokubauskas, IFF-6/2, KTU IF. 2017",
		L"About", MessageBoxButtons::OK, MessageBoxIcon::Information);
	DialogResult = Windows::Forms::DialogResult::None;
}
};
}
