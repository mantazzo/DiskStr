[![pipeline status](https://gitlab.com/mantazzo/DiskStr/badges/master/pipeline.svg)](https://gitlab.com/mantazzo/DiskStr/commits/master) [![Build status](https://ci.appveyor.com/api/projects/status/ex5hk37wlgouig9p?svg=true)](https://ci.appveyor.com/project/mantazzo/diskstr)

# Diskrečiosios struktūros

A simple project I made for this subject in my university.

- Project Language: C++ (CLR)
- Project Text Language: English
- Tools used for making this project: Microsoft Visual Studio Community 2017
- Required: .NET Framework v4.5

### Project finished on: 2017-12-18